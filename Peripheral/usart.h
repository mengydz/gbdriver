/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "FIFOBuffer.h"

#define USART_TX_BUFFER_SIZE     50              //USART BUFFER FIFO SIZE
#define USART_RX_BUFFER_SIZE     50              //USART BUFFER FIFO SIZE

/* USER CODE BEGIN Includes */
class USART
{
public:
	USART(USART_TypeDef* usart=USART1,uint32_t baudrate=115200);
	bool SendByte(uint8_t data);
	bool SendBytes(uint8_t* txData,uint16_t size);
	bool GetBytes(uint8_t data[], uint16_t num);
	bool GetByte(uint8_t &data);
	uint16_t RxSize();
	uint16_t TxSize();
	uint16_t TxOverflowSize();
	uint16_t RxOverflowSize();
	void ClearRxBuf();
	void ClearTxBuf();
	USART& operator<<(int val);
	USART& operator<<(double val);
	USART& operator<<(const char* pStr);
	USART& operator<<(uint8_t* pStr);
	USART& operator<<(const uint8_t* pStr);
	bool CheckFrame(DataFrame &df);
	void RxEnable();
	void RxDisable();
	void TxEnable();
	void TxDisable();
	void IRQ();
private:
	bool isBusySend;

	USART_TypeDef* _usart;
	UART_HandleTypeDef _huart;
	FIFOBuffer<uint8_t, USART_TX_BUFFER_SIZE>  TxBuf;  //USART Tx Buffer
	FIFOBuffer<uint8_t, USART_RX_BUFFER_SIZE>  RxBuf;  //USART Rx Buffer
	uint8_t Precision;   //when show precision after dot "."  when use "<<" to show float value

	uint16_t TxOverflow; //Tx overflow byte count
	uint16_t RxOverflow; //Rx overflow byte count

	void RCC_Configuration();
	void NVIC_Configuration();
};


#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
