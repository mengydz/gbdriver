/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

TIMER *pTIMER1,*pTIMER2,*pTIMER3,*pTIMER4,*pTIMER5,*pTIMER6,*pTIMER7,*pTIMER8;
/* USER CODE BEGIN 0 */

TIMER::TIMER(TIM_TypeDef *timer,uint32_t prescaler,uint32_t period,uint32_t countermode):
_timer(timer)
{
	RCC_Configuration();
	NVIC_Configuration();

	_htim.Instance = timer;
	_htim.Init.Prescaler = prescaler;
	_htim.Init.CounterMode = countermode;
	_htim.Init.Period = period;
	_htim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	_htim.Init.RepetitionCounter = 0;
	if (HAL_TIM_PWM_Init(&_htim) != HAL_OK)
	{

	}
}

void TIMER::RCC_Configuration()
{
	if(_timer == TIM5)
	{
		__HAL_RCC_TIM5_CLK_ENABLE();
	}
}

void TIMER::NVIC_Configuration()
{
	if(_timer == TIM5)
	{
		__HAL_RCC_TIM5_CLK_ENABLE();
	    HAL_NVIC_SetPriority(TIM5_IRQn, 6, 0);
	    HAL_NVIC_EnableIRQ(TIM5_IRQn);
	    pTIMER5 = this;
	}
}

void TIMER::Enable()
{
	HAL_TIM_Base_Start(&_htim);
}

void TIMER::Disable()
{
	HAL_TIM_Base_Stop(&_htim);
}

void TIMER::EnableIT()
{
	HAL_TIM_Base_Start_IT(&_htim);
}

void TIMER::DisableIT()
{
	HAL_TIM_Base_Stop_IT(&_htim);
}

uint64_t TIMER::GetMicro()
{
	uint64_t cnt;
	cnt=_timer->CNT;
	return glo_50ms*50000+cnt;
}

uint32_t TIMER::GetMillis()
{
	uint32_t cnt;
	cnt=_timer->CNT;
	return glo_50ms*50+cnt/1000;
}

void TIMER::IRQ()
{
	if((_htim.Instance->DIER & TIM_IT_UPDATE) == TIM_IT_UPDATE)
	{
		_htim.Instance->SR &= ~TIM_IT_UPDATE;
		glo_50ms++;
	}
}

extern "C"{
void TIM5_IRQHandler(void)
{
  /* USER CODE BEGIN TIM7_IRQn 0 */
	pTIMER5->IRQ();
  /* USER CODE END TIM7_IRQn 1 */
}
}
/* USER CODE END 0 */
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
