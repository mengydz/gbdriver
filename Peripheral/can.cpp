/**
  ******************************************************************************
  * File Name          : CAN.c
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

CAN *pCAN1,*pCAN2;

CAN::CAN(CAN_TypeDef* can,uint32_t prescaler,uint32_t sjw,uint32_t bs1,uint32_t bs2):
_can(can)
{
	CAN_FilterTypeDef  sFilterConfig;

	RCC_Configuration();
	NVIC_Configuration();

	hcan.Instance = CAN1;
	hcan.Init.Prescaler = prescaler;
	hcan.Init.Mode = CAN_MODE_NORMAL;
	hcan.Init.SyncJumpWidth = sjw;
	hcan.Init.TimeSeg1 = bs1;
	hcan.Init.TimeSeg2 = bs2;
	hcan.Init.TimeTriggeredMode = DISABLE;
	hcan.Init.AutoBusOff = DISABLE;
	hcan.Init.AutoWakeUp = DISABLE;
	hcan.Init.AutoRetransmission = DISABLE;
	hcan.Init.ReceiveFifoLocked = DISABLE;
	hcan.Init.TransmitFifoPriority = DISABLE;
	if (HAL_CAN_Init(&hcan) != HAL_OK)
	{

	}

	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if(HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
	{

	}

}

void CAN::RCC_Configuration()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(_can==CAN1)
	{
		__HAL_RCC_CAN1_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
	else if(_can==CAN2)
	{
		__HAL_RCC_CAN2_CLK_ENABLE();
		__HAL_RCC_CAN1_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_CAN2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}
}
void CAN::NVIC_Configuration()
{
	if(_can==CAN1)
	{
		HAL_NVIC_SetPriority(CAN1_TX_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
		HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
		HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
		pCAN1 = this;
	}
	else if(_can==CAN2)
	{
		HAL_NVIC_SetPriority(CAN2_TX_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN2_TX_IRQn);
		HAL_NVIC_SetPriority(CAN2_RX0_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
		HAL_NVIC_SetPriority(CAN2_RX1_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(CAN2_RX1_IRQn);
		pCAN2 = this;
	}
}

void CAN::Start()
{
	/*##-3- Start the CAN peripheral ###########################################*/
	if (HAL_CAN_Start(&hcan) != HAL_OK)
	{

	}
}

void CAN::Stop()
{
	/*##-3- Start the CAN peripheral ###########################################*/
	if (HAL_CAN_Start(&hcan) != HAL_OK)
	{

	}
}

void CAN::Test()
{
	hTx.StdId = 0x11;
	hTx.RTR = CAN_RTR_DATA;
	hTx.IDE = CAN_ID_STD;
	hTx.DLC = 8;
	hTx.TransmitGlobalTime = DISABLE;
	TxData[0] = 0x01;
	TxData[1] = 0x02;
	TxData[2] = 0x03;
	TxData[3] = 0x04;
	TxData[4] = 0x05;
	TxData[5] = 0x06;
	TxData[6] = 0x07;
	TxData[7] = 0x08;

	/* Request transmission */
	if(HAL_CAN_AddTxMessage(&hcan, &hTx, TxData, &TxMailbox) != HAL_OK)
	{

	}

	/* Wait transmission complete */
	while(HAL_CAN_GetTxMailboxesFreeLevel(&hcan) != 3) {}
}

void CAN::TX_IRQ()
{
	HAL_CAN_IRQHandler(&hcan);
}

void CAN::RX0_IRQ()
{
	HAL_CAN_IRQHandler(&hcan);
}

void CAN::RX1_IRQ()
{
	HAL_CAN_IRQHandler(&hcan);
}

extern "C"
{
void CAN1_TX_IRQHandler(void)
{
	pCAN1->TX_IRQ();
}
void CAN1_RX0_IRQHandler(void)
{
	pCAN1->RX0_IRQ();
}
void CAN1_RX1_IRQHandler(void)
{
	pCAN1->RX1_IRQ();
}
void CAN2_TX_IRQHandler(void)
{
	pCAN2->TX_IRQ();
}
void CAN2_RX0_IRQHandler(void)
{
	pCAN2->RX0_IRQ();
}
void CAN2_RX1_IRQHandler(void)
{
	pCAN2->RX1_IRQ();
}
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
