/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */
GPIO::GPIO(GPIO_TypeDef *port,uint16_t pin,uint32_t mode,\
		uint32_t pulltype,uint32_t speed,GPIO_PinState state):
mPin(pin),mPort(port),mSpeed(speed)
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  HAL_GPIO_WritePin(port, pin, state);

	  RCC_Configuration();
	  GPIO_InitStruct.Pin = pin;
	  GPIO_InitStruct.Mode = mode;
	  GPIO_InitStruct.Pull = pulltype;
	  GPIO_InitStruct.Speed = speed;
	  HAL_GPIO_Init(port, &GPIO_InitStruct);
}

void GPIO::RCC_Configuration()
{
	if(mPort == GPIOA)
		__HAL_RCC_GPIOA_CLK_ENABLE();
	else if(mPort == GPIOB)
		__HAL_RCC_GPIOB_CLK_ENABLE();
	else if(mPort == GPIOC)
		__HAL_RCC_GPIOC_CLK_ENABLE();
	else if(mPort == GPIOD)
		__HAL_RCC_GPIOD_CLK_ENABLE();
	else if(mPort == GPIOE)
		__HAL_RCC_GPIOE_CLK_ENABLE();
	else if(mPort == GPIOF)
		__HAL_RCC_GPIOF_CLK_ENABLE();
	else if(mPort == GPIOG)
		__HAL_RCC_GPIOG_CLK_ENABLE();
	else if(mPort == GPIOH)
		__HAL_RCC_GPIOH_CLK_ENABLE();
}

void GPIO::SetLevel(uint8_t level)
{
	if(level)
		HAL_GPIO_WritePin(mPort, mPin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(mPort,mPin,GPIO_PIN_RESET);
}

uint8_t GPIO::GetLevel()
{
	if(HAL_GPIO_ReadPin(mPort,mPin))
		return 1;
	else
		return 0;
}

void GPIO::Toggle()
{
	HAL_GPIO_TogglePin(mPort,mPin);
}
/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
