/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include "string.h"

USART *pUSART1,*pUSART2,*pUSART3,*pUSART4,*pUSART5,*pUSART6;
/* USER CODE BEGIN 0 */


USART::USART(USART_TypeDef* usart,uint32_t baudrate):
_usart(usart)
{
	TxOverflow = 0;
	RxOverflow = 0;

	RCC_Configuration();
	NVIC_Configuration();

	_huart.Instance = usart;
	_huart.Init.BaudRate = baudrate;
	_huart.Init.WordLength = UART_WORDLENGTH_8B;
	_huart.Init.StopBits = UART_STOPBITS_1;
	_huart.Init.Parity = UART_PARITY_NONE;
	_huart.Init.Mode = UART_MODE_TX_RX;
	_huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	_huart.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&_huart) != HAL_OK)
	{

	}
//	__HAL_UART_ENABLE(&_huart);
}

void USART::RCC_Configuration()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if(_usart == USART1)
	{
		__HAL_RCC_GPIOB_CLK_ENABLE();
		__HAL_RCC_USART1_CLK_ENABLE();

	    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Pull = GPIO_PULLUP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
	    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}else if(_usart == USART2)
	{
		__HAL_RCC_USART2_CLK_ENABLE();
	}else if(_usart == USART3)
	{
		__HAL_RCC_USART3_CLK_ENABLE();
	}else if(_usart == UART4)
	{
		__HAL_RCC_UART4_CLK_ENABLE();
	}else if(_usart == UART5)
	{
		__HAL_RCC_UART5_CLK_ENABLE();
	}else if(_usart == USART6)
	{
		__HAL_RCC_USART6_CLK_ENABLE();
	}
}

void USART::NVIC_Configuration()
{
	if(_usart == USART1)
	{
		HAL_NVIC_SetPriority(USART1_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(USART1_IRQn);
		pUSART1 = this;
	}else if(_usart == USART2)
	{
		HAL_NVIC_SetPriority(USART2_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(USART2_IRQn);
		pUSART2 = this;
	}else if(_usart == USART3)
	{
		HAL_NVIC_SetPriority(USART3_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(USART3_IRQn);
		pUSART3 = this;
	}else if(_usart == UART4)
	{
		HAL_NVIC_SetPriority(UART4_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(UART4_IRQn);
		pUSART4 = this;
	}else if(_usart == UART5)
	{
		HAL_NVIC_SetPriority(UART5_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(UART5_IRQn);
		pUSART5 = this;
	}else if(_usart == USART6)
	{
		HAL_NVIC_SetPriority(USART6_IRQn, 7, 0);
		HAL_NVIC_EnableIRQ(USART6_IRQn);
		pUSART6 = this;
	}
}

void USART::RxEnable()
{
	__HAL_UART_ENABLE_IT(&_huart,UART_IT_RXNE);
}

void USART::TxEnable()
{
	__HAL_UART_ENABLE_IT(&_huart,UART_IT_TXE);
}

void USART::RxDisable()
{
	__HAL_UART_DISABLE_IT(&_huart,UART_IT_RXNE);
}

void USART::TxDisable()
{
	__HAL_UART_DISABLE_IT(&_huart,UART_IT_TXE);
}

bool USART::SendByte(uint8_t data)
{
	if (TxBuf.Put(data))
		return true;
	TxOverflow++;
	return false;
}

bool USART::SendBytes(uint8_t* txData,uint16_t size)
{
	if (TxBuf.ResSize() < size)      //compare the unused bytes and sending bytes
	{
		TxOverflow += size;         //flash Tx overflow bytes
		return false;
	}
	TxBuf.Puts(txData, size);                        //add data to Tx buffer, if overflow, return false
	if (isBusySend)                return true;       //USARTx is busy send data, return
	if (TxBuf.Size() <= 0)        return true;       //have no data to send, return
	isBusySend = true;                               //set busy state, going to send data

	__HAL_UART_ENABLE_IT(&_huart,UART_IT_TXE);
	return true;
}

bool USART::GetBytes(uint8_t data[], uint16_t num)
{
	return RxBuf.Gets(data, num);
}
bool USART::GetByte(uint8_t &data)
{
	return RxBuf.Get(data);
}

uint16_t USART::RxSize()
{
	return RxBuf.Size();
}
uint16_t USART::TxSize()
{
	return TxBuf.Size();
}
uint16_t USART::TxOverflowSize()
{
	return TxOverflow;
}
uint16_t USART::RxOverflowSize()
{
	return RxOverflow;
}
void USART::ClearRxBuf()
{
	RxBuf.Clear();
}
void USART::ClearTxBuf()
{
	TxBuf.Clear();
}

USART& USART::operator<<(int val)
{
	uint8_t sign = 0, len = 0, data[10];
	if (val < 0)
	{
		sign = 1;
		val = -val;
	}
	do
	{
		len++;
		data[10 - len] = val % 10 + '0';
		val = val / 10;
	} while (val);
	if (sign == 1)
		data[10 - (++len)] = '-';
	SendBytes(data + 10 - len, len);
	return *this;
}
USART& USART::operator<<(double val)
{
	uint8_t sign = 0, len = 0, data[20];
	if (val < 0)
	{
		sign = 1;
		val = -val;
	}
	uint8_t prec = Precision;
	while (prec--)
		val *= 10;
	uint32_t t = val;
	do
	{
		if (++len == Precision + 1) data[20 - len] = '.';
		else
		{
			data[20 - len] = t % 10 + '0';
			t = t / 10;
		}
	} while (t || len < Precision + 2);
	//if(len==3) data[20-(++len)] = '.';
	//if(len==4) data[20-(++len)] = '0';
	if (sign == 1)
		data[20 - (++len)] = '-';
	SendBytes(data + 20 - len, len);
	return *this;
}
USART& USART::operator<<(const char* pStr)
{
	unsigned int length = 0;
	for (int i = 0; pStr[i] != '\0'; ++i)
	{
		++length;
	}
	SendBytes((uint8_t*)pStr, length);
	return *this;
}

USART& USART::operator<<(uint8_t* pStr)
{
	SendBytes((uint8_t*)pStr, strlen((char *)pStr));
	return *this;
}

USART& USART::operator<<(const uint8_t* pStr)
{
	SendBytes((uint8_t*)pStr, strlen((char *)pStr));
	return *this;
}

bool USART::CheckFrame(DataFrame &df)
{
	return RxBuf.CheckFrame(df);;
}

void USART::IRQ()
{
	uint8_t _data;
	/* 处理接收中断  */
	if((__HAL_UART_GET_FLAG(&_huart,UART_FLAG_RXNE) != RESET))
	{
		/* 从串口接收数据寄存器读取数据存放到接收FIFO */
		_data = _huart.Instance->DR;//读DR会自动清空中断标志位
		if (!RxBuf.Put(_data))        //receive byte
		{
			RxOverflow++;
		}
	}

	/* 处理发送缓冲区空中断 */
	if(__HAL_UART_GET_FLAG(&_huart,UART_FLAG_TXE))
	{
		if (TxBuf.Size() > 0)                             //still left some bytes of data
		{
			TxBuf.Get(_data);                                //get one byte data from buffer
			_huart.Instance->DR = (_data & (uint16_t)0x01FF);              //send one byte data
		}
		else                                               //all data send complete
		{
			__HAL_UART_CLEAR_FLAG(&_huart,UART_FLAG_TXE);
			__HAL_UART_DISABLE_IT(&_huart, UART_IT_TXE);
			isBusySend = false;                              //set free state
		}
	}
}

extern "C"
{
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
	pUSART1->IRQ();
  /* USER CODE END USART1_IRQn 1 */
}
}
/* USER CODE END 0 */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
