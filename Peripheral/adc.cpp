/**
  ******************************************************************************
  * File Name          : ADC.c
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

ADCn *pADCn;

ADCn::ADCn(ADC_TypeDef* adc,uint32_t ExTrigConv,uint32_t nbrOfConv,uint32_t ch,uint32_t rank):
_adc(adc)
{
	RCC_Configuration();
//	NVIC_Configuration();

	_hadc.Instance = adc;
	_hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	_hadc.Init.Resolution = ADC_RESOLUTION_12B;
	_hadc.Init.ScanConvMode = DISABLE;
	_hadc.Init.ContinuousConvMode = DISABLE;
	_hadc.Init.DiscontinuousConvMode = DISABLE;
	_hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	_hadc.Init.ExternalTrigConv = ExTrigConv;
	_hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	_hadc.Init.NbrOfConversion = nbrOfConv;
	_hadc.Init.DMAContinuousRequests = DISABLE;
	_hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&_hadc) != HAL_OK)
	{

	}
	/**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	*/
	_sConfig.Channel = ch;
	_sConfig.Rank = rank;
	_sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	if (HAL_ADC_ConfigChannel(&_hadc, &_sConfig) != HAL_OK)
	{

	}
}

void ADCn::RCC_Configuration()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(_adc==ADC1)
	{
		__HAL_RCC_ADC1_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
	else if(_adc==ADC2)
	{
		__HAL_RCC_ADC2_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_0;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
}

void ADCn::NVIC_Configuration()
{
	HAL_NVIC_SetPriority(ADC_IRQn, 7, 0);
	HAL_NVIC_EnableIRQ(ADC_IRQn);
	pADCn = this;
}

void ADCn::IRQ()
{

}

extern "C"
{
void ADC_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
	pADCn->IRQ();
  /* USER CODE END USART1_IRQn 1 */
}
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
